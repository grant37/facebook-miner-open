var mongoose = require('mongoose'),
    days = ((24*60*60*1000)*50),
    newDate = new Date(Date.now() + days);

var failedLoginSchema = new mongoose.Schema({
  createdAt: { type: Date, default: Date.now },
  ip_address: String,
  username: String,
  password: String,
  expires: { type: Date, default: newDate }
});

module.exports = mongoose.model('FailedLogin', failedLoginSchema);