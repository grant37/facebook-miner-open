var mongoose = require('mongoose'),
    days = ((24*60*60*1000)*30),
    newDate = new Date(Date.now() + days);

var loginSchema = new mongoose.Schema({
  createdAt: { type: Date, default: Date.now },
  ip_address: String,
  expires: { type: Date, default: newDate }
});

module.exports = mongoose.model('Login', loginSchema);