var mongoose = require('mongoose'),
    Mixed = mongoose.Schema.Types.Mixed;

var userSchema = new mongoose.Schema({
  fbid: { type: String, unique: true },
  fbToken: { type: String },
  minerDone: { type: Boolean, default: false },
  surveyDone: { type: Boolean, default: false },
  surveyPages: {},
  //facebook: { type: String, unique: true, sparse: true },
});

module.exports = mongoose.model('User', userSchema);