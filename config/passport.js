var passport = require('passport'),
    FacebookStrategy = require('passport-facebook').Strategy,
    BasicStrategy = require('passport-http').BasicStrategy,
    User = require('../models/User'),
    FailedLogin = require('../models/FailedLogin'),
    secrets = require('./secrets'),
    crypto = require("crypto");
    
passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  User.findById(id, function(err, user) {
    done(err, user);
  });
});

passport.use(new FacebookStrategy(secrets.facebook, function(req, accessToken, refreshToken, profile, done) {
  var idHash = crypto.createHash('md5').update(profile.id).digest('hex').toString();
  
  //console.log(accessToken, refreshToken);
  
  if (req.user) {
    
    User.findOne({ fbid: idHash }, function(err, existingUser) {
      if (existingUser) {
        
        if (existingUser.fbToken !== accessToken) {
          existingUser.fbToken = accessToken;
          existingUser.save(function(err) {
            if (err) { console.log(err); }
          });
        }
        done(err);
      } else {
        User.findById(req.user.id, function(err, user) {
          user.fbid = idHash;
          user.fbToken = accessToken;
          user.save(function(err) {
            done(err, user);
          });
        });
      }
    });
  } else {
    User.findOne({ fbid: idHash }, function(err, existingUser) {
      if (existingUser) {
        
        if (existingUser.fbToken !== accessToken) {
          existingUser.fbToken = accessToken;
          existingUser.save(function(err) {
            if (err) { console.log(err); }
          });
        }
        
        return done(null, existingUser);
      }
      var user = new User();
      user.fbid = idHash;
      user.fbToken = accessToken;
      user.save(function(err) {
        done(err, user);
      });
    });
  }
}));

passport.use(new BasicStrategy({ passReqToCallback: true }, function(req, user, pass, done) {
    if (user.valueOf() === 'admin' && pass.valueOf() === secrets.basicAuth) {
      return done(null, true);
    }
    else {
      // log failed login attempt
      var ip = req.ip;
      
      var log = new FailedLogin({
        ip_address: ip,
        username: user.valueOf(),
        password: pass.valueOf()
      });
      
      log.save(function(err) {
        if (err) {
          console.error(err);
          return done(err, false);
        }
        else {
          return done(null, false);
        }
      });
    }
  }
));

exports.isAuthenticated = function(req, res, next) {
  if (req.isAuthenticated()) return next();
  res.redirect('/');
};

exports.isAuthorized = function(req, res, next) {
  //var provider = req.path.split('/').slice(-1)[0];
  if (req.user.fbToken) next();
  else res.redirect('/auth/facebook');
};