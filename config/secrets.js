module.exports = {
  //db: 'localhost',
  db: 'mongodb://changed:changed@oceanic.mongohq.com:10035/fb_miner',
  localAuth: true,
  sessionSecret: "changed",
  basicAuth: "(Changed For Release)",
  
  facebookAuth: true,
  facebook: {
    // New Config - Need to get app approved
    clientID: '458294894320462',
    clientSecret: '8f12349744bcb2a1bdd936d450193d0e',
    
    // Old Config - Still Works
    //clientID: '1425747914335137',
    //clientSecret: 'd6289d488c64b398a82ed4ff2c1d313a',
    //callbackURL: 'https://fbapp-c9-tgrant15.c9.io/auth/facebook/callback',
    callbackURL: 'http://purdue.herokuapp.com/auth/facebook/callback',
    passReqToCallback: true
  }
};