$(document).foundation();

;(function() {
  
  $("[data-agree]").addClass('disabled').bind('click', function(e) {
    e.preventDefault();  
  });
  
  $("[name=agree]").change(function() {
    if (this.checked) {
      $("[data-agree]").removeClass('disabled').unbind('click');
    }
    else {
      $("[data-agree]").addClass('disabled').bind('click', function(e) {
        e.preventDefault();  
      });
    }
  });

})();