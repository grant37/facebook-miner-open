var spawn = require('child_process').spawn,
    AWS = require('aws-sdk'),
    graph = require('fbgraph'),
    User = require('../models/User');

var s3 = new AWS.S3();

/**
 * miner
 * 
 */
exports.miner = function(uidHash, token, socket) {
 var miner = spawn('node', ['miner.js', uidHash, token]);
  
  miner.stdout.setEncoding('utf8');
  miner.stdout.on('data', function(data) {
    socket.emit('data', { Body: data });
    console.log(data);
  });
  miner.stderr.on('data', function(data) {
    socket.emit('data', { Body: data });
    console.log('stderr: '+data);
  });
  miner.on('close', function(code) {
    if (code === 0) {
      socket.emit('close', { status: 'success' });
      console.log('Miner finished successfully.');
    }
    else {
      socket.emit('close', { status: 'fail', code: code });
      console.log('Miner failed to execute successfully. Ended with code: '+code);
    }
  });
};

/**
 * updateMiner
 * Updates the user's document to show that the miner finished.
 * 
 * @param uidHash
 */
exports.updateMiner = function(uidHash) {
  User.findOneAndUpdate({fbid: uidHash }, { minerDone: true }, function(err, user) {
    if (err) {
      console.error(err);
      return false;
    }
  });
  return true;
};

/**
 * updateSurvey
 * Updates the user's document to show that the survey was completed
 * 
 * @param uidHash - String
 * @param responseId - String (Response ID from qualtrics survey)
 */
exports.updateSurvey = function(uidHash) {
  User.findOneAndUpdate({fbid: uidHash }, { surveyDone: true }, function(err, user) {
    if (err) {
      console.error(err);
      return false;
    }
  });
  return true;
};

/**
 * completed
 * Checks to see if both the survey & mining is completed for a user
 *
 * @param uidHash - String
 */
exports.completed = function(uidHash) {
  return User.findOne({fbid: uidHash}, function(err, user) {
    if (err) {
      console.error(err);
      return false;
    }
    if (!user) {
      console.log('no such user');
      return false;
    }
    else {
      var mineStatus = user.minerDone,
          surveyStatus = user.surveyDone;
          
      if (mineStatus && surveyStatus) {
        return true;
      }
      else {
        return false;
      }
    }
  });
};

/**
 * combineData
 */
exports.combineData = function(uid, uidHash) {
  return User.findById(uid, function(err, user) {
    if (err) {
      console.error(err);
      return false;
    }
    if (!user) {
      console.error('No user');
      return false;
    }
    else {
      
      var params = {
        Bucket: 'fbminer',
        Key: uidHash+".json",
      };
      
      s3.getObject(params, function(err, data) {
        if (err) {
          console.error(err);
        }
        else {
          var userData = JSON.parse(data.Body),
              responseData = user.surveyPages;
              
          var newData = {
            pages: responseData
          };
          
          userData.survey = newData;
          
          var params = {
            Bucket: 'fbminer',
            Key: uidHash+'.json',
            ACL: 'public-read-write',
            Body: JSON.stringify(userData, null, 2)
          };
          
          s3.putObject(params, function(err, data) {
            if (err) return console.error(err);
            
            user.surveyPages = {};
            user.minerDone = false;
            user.surveyDone = false;
            
            user.save(function(err) {
              if (err) return console.error(err);
            });
          });
        }
      });
    
    }
    
  });

};