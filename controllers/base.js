var AWS = require('aws-sdk'),
    sHelper = require('../helper_modules/survey'),
    apiController = require('../controllers/api'),
    graph = require('fbgraph'),
    User = require('../models/User'),
    AdmZip = require('adm-zip');

var s3 = new AWS.S3();

/**
 * GET /
 */
exports.getHome = function(req, res) {
  res.render("home", {
    title: "Home"
  });
};

/**
 * GET /start
 * After user is logged in, show the mining status
 */
exports.getStart = function(req, res) {
  var uidHash = req.user.fbid,
      token = req.user.fbToken;
  
  /** 
   * Need to check here if the user has already completed the survey, but there
   * was an error mining their FB data, so we will send them to another page that
   * will just mine and combine their data.
   */
  User.findById(req.user.id, function(err, user) {
    if (err) {
      console.error(err);
    }
    if (!user) {
      console.error('No such user exists');
    }
    else {
      
      var minerDone = user.minerDone,
          surveyDone = user.surveyDone;
          
      if (minerDone === false && surveyDone === true) {
        /**
         * The miner failed for the user, need to send to different page
         * to get the mining data.
         */
        res.render("miner", {
          title: "Start",
          incSocketIO: true,
          restartMiner: true,
          datas: { hash: uidHash, token: token }
        });
      }
      
      if (minerDone === true && surveyDone === false) {
        /**
         * The miner finished successfully, but the user did not complete the
         * survey. We may want to have them explicity take the survey, or we 
         * can just restart the whole process.
         * 
         * For now, just restart the whole process.
         */
        
        res.render("start", {
          title: "Start",
          incSocketIO: true,
          mineStart: true,
          datas: { hash: uidHash, token: token }
        });
      }
      
      if (minerDone === false && surveyDone === false) {
        /**
         * Either the user has already participated, and everything was completed
         * successfully, or it is a new user that needs to start the process.
         */
         
        res.render("start", {
          title: "Start",
          incSocketIO: true,
          mineStart: true,
          datas: { hash: uidHash, token: token }
        });
      }
      
    }
  });
};

/**
 * GET /callback
 * Callback after the survey is taken. From qualtrics
 */
exports.getSurveyComplete = function(req, res) {
  var uidHash = req.query.uid;
  
  res.render('wait', {
    title: 'Please Wait',
    incSocketIO: true,
    surveyFinish: true,
    datas: { uid: uidHash }
  });
};

/**
 * GET /success
 */
exports.getSuccess = function(req, res) {
  res.render("success", {
    title: "Success: Process Finished",
    hash: req.user.fbid
  });
};

/**
 * GET /error
 */
exports.getError = function(req, res) {
  res.render("error", {
    title: "Error"
  });
};

/**
 * GET /results
 */
exports.getResults = function(req, res) {
  s3.listObjects({Bucket: "fbminer"}, function(err, data) {
    if (err) {
      console.error(err);
      res.render("results", {
        title: "Results",
        results: []
      });
    }
    else {
      res.render("results", {
        title: "Results",
        results: data.Contents
      });
    }
  });
};

/**
 * GET /results/:awsKey
 * Gets the data result from AWS s3 storage
 */
exports.getResultByKey = function(req, res) {
  s3.getObject({Bucket: "fbminer", Key: req.params.awsKey}, function(err, data) {
    if (err) {
      console.error(err);
    }
    else {
      res.send(data.Body);
    }
  });
};

/**
 * GET /results/export
 * Gets all the data files in a zip file.
 */
exports.exportData = function(req, res) {
  s3.listObjects({Bucket: "fbminer"}, function(err, data) {
    if (err) {
      console.error(err);
      res.send(500);
    }
    else {
      
      var results = data.Contents,
          zip = new AdmZip();
          
      for (var a = 0; a < results.length; a++) {
        var resultKey = results[a].Key;
        
        s3.getObject({Bucket: "fbminer", Key: resultKey}, function(err, data) {
          if (err) {
            console.error(err);
          }
          else {
            zip.addFile(resultKey, data.Body);
          }
        });
      }
      
      var resultFile = zip.toBuffer();
      
      res.send(resultFile);
      
    }
  });
};

/**
 * GET /logout
 */
exports.getLogout = function(req, res) {
  if (req.user) {
    req.logout();
  }
  
  res.redirect('/');
};

/**
 * GET /deauth
 */
exports.getDeauth = function(req, res) {
  if (req.user) {
    graph.setAccessToken(req.user.fbToken);
    
    graph.get('me?fields=id', function(err, uidRes) {
      if (err) {
        console.error('Deauth error: '+err);
        return res.redirect('/finish');
      }
      if (uidRes) {
        var uid = uidRes.id;
        // De-authorize
        graph.del(uid+'/permissions', function(err, deRes) {
          req.logout();
          if (req.headers && req.headers.authorization) { delete req.headers.authorization; }
          return res.redirect('/finish');
        });
      }
      else {
        console.log('Did not get uidRes');
        return res.redirect('/finish');
      }
    });
  }
  else {
    return res.redirect('/finish');
  }
};

/**
 * GET /finish
 */
exports.getFinish = function(req, res) {
  res.render("finish", {
    title: "Finished"
  });
};

/**
 * GET /survey
 */
exports.getSurvey = function(req, res) {
  var page = req.query.p || 1,
      qObj = sHelper.getPageQuestions(page),
      lastPage = false;
  
  if (page == 1) {    
    User.findById(req.user.id, function(err, user) {
      if (err) {
        console.error(err);
        res.redirect('/error');
      }
      else if (!user) {
        console.error(err);
        res.redirect('/error');
      }
      else {
        user.surveyPages = {};
        user.markModified('surveyPages');
        
        user.save(function(err) {
          if (err) {
            console.error(err);
            res.redirect('/error');
          }
        });
      }
    });
  }
  
  if (page == 21) {
    lastPage = true;
  }

  if (page >= 1 && page <= 21) {
    res.render("survey", {
      title: "Survey",
      styleSheets: ['css/survey.css'],
      questions: qObj.questions,
      isLastPage: lastPage
    });
  }
  else {
    res.redirect('/error');
  }
};

/**
 * POST /survey
 */
exports.postSurvey = function(req, res) {
  var page = req.query.p || 1,
      qObj =sHelper.getPageQuestions(page),
      responses = sHelper.parseResponse(req.body, qObj.questions),
      rObj = {};
      
  rObj[page] = { questions: responses };
  
  User.findById(req.user.id, function(err, user) {
    if (err) {
      console.error(err);
      res.redirect('/error');
    }
    else if (!user) {
      console.error(err);
      res.redirect('/error');
    }
    else {
      user.surveyPages[page] = { questions: responses };
      user.markModified('surveyPages');
      
      user.save(function(err) {
        if (err) {
          console.error(err);
        }
        
        if (page >= 21) {
          var nextSurvey = 'https://purdue.qualtrics.com/SE/?SID=SV_3dvYpwgbIorOx0h&uid='+req.user.fbid;
          res.redirect(nextSurvey);
        }
        else {
          var npage = Number(page) + 1;
          res.redirect('/survey?p='+npage);
        }
      });
    }
  });
};

exports.doFix = function(req, res) {
  var uid = req.query.id,
      uidHash = req.query.idh;
  
  console.log(uid, uidHash);
   
  if (uid === undefined || uidHash === undefined) {
    res.send(500);
  }
  else {
    var fix = apiController.combineData(uid, uidHash);
    
    if (fix) {
      res.send(9000, 'Awwwwww Yesssss');
    }
  }
};