var async = require('async'),
    graph = require('fbgraph'),
    crypto = require('crypto'),
    AWS = require('aws-sdk'),
    moment = require('moment');
    
var s3 = new AWS.S3();
    
// .contains polyfill  
if ( !String.prototype.contains ) {
  String.prototype.contains = function() {
    return String.prototype.indexOf.apply( this, arguments ) !== -1;
  };
}

/**
 * Miner Vars
 */
var uidHash = process.argv[2],
    token = process.argv[3],
    s3UserFile = uidHash+".json",
    sixMonthsAgo = moment().subtract('years', 1).toDate(),
    nameVars = [],
    collected = {};

graph.setAccessToken(token);

console.log('Starting mining data for uid: '+uidHash);

async.series({
  gatherBasicInfo: function(done) {
    var graphStr = "me?fields=location,political,religion,relationship_status,name,friends,gender";
    graph.get(graphStr, function(err, res) {
      if (err) {
        console.error('gatherBasicInfo Error: '+err);
        return done(err);
      }
      if (res) {
        // get variations of name
        var nA = res.name.split(" "),
            first = nA[0], last = nA[1],
            fInitialLast = first[0]+last;
        
        nameVars.push(first);
        nameVars.push(first.toLowerCase());
        nameVars.push(first.toUpperCase());
        nameVars.push(last);
        nameVars.push(last.toLowerCase());
        nameVars.push(last.toUpperCase());
        nameVars.push(fInitialLast);
        nameVars.push(fInitialLast.toLowerCase());
        nameVars.push(fInitialLast.toUpperCase());
        
        var state, country = null;
        
        if (res.location === null) {
          state = null;
        }
        else {
          var cityState = res.location.name.split(", ");
          state = cityState[1];
        }
        
        var storeUser = {
          "sex": res.gender,
          "friend_count": res.friends.summary.total_count,
          "current_location": {
            "state" : state,
            "country": country
          },
          "relationship_status": res.relationship_status,
          "political" : res.political,
          "religion" : res.religion,
          "uid" : uidHash
        };
        
        collected.user = storeUser;
        console.log('Finished mining basic user info.');
        done(err, storeUser);
        
      }
      else {
        console.log(err, res);
        done(err, null);
      }
    });
    
  },
  /**
   * Old gatherBasicInfo - Will only work if API version is 2.0 or below
  gatherBasicInfo: function(done) {
    var qStr = "SELECT name, sex, friend_count, current_location.state, current_location.country, relationship_status, political, religion FROM user WHERE uid = me()";
    graph.fql(qStr, function(err, res) {
      if (err) {
        console.error(err);
        return done(err);
      }
      if (res.data.length > 0) {
        // get variations of name
        var nA = res.data[0].name.split(" "),
            first = nA[0], last = nA[1],
            fInitialLast = first[0]+last;
        
        nameVars.push(first);
        nameVars.push(first.toLowerCase());
        nameVars.push(first.toUpperCase());
        nameVars.push(last);
        nameVars.push(last.toLowerCase());
        nameVars.push(last.toUpperCase());
        nameVars.push(fInitialLast);
        nameVars.push(fInitialLast.toLowerCase());
        nameVars.push(fInitialLast.toUpperCase());
        
        
        var state, country;
        
        if (res.data[0].current_location === null) {
          state = null;
          country = null;
        }
        else {
          state = res.data[0].current_location.state;
          country = res.data[0].current_location.country;
        }
        
        var storeUser = {
          "sex": res.data[0].sex,
          "friend_count": res.data[0].friend_count,
          "current_location": {
            "state" : state,
            "country": country
          },
          "relationship_status": res.data[0].relationship_status,
          "political" : res.data[0].political,
          "religion" : res.data[0].religion,
          "uid" : uidHash
        };
        
        collected.user = storeUser;
        console.log('Finished mining basic user info.');
        done(err, storeUser);
      }
      else {
        console.log(err, res);
        done(err, null);
      }
    });
  },
  */
  gatherComments: function(done) {
    var graphStr = "me/friends?fields=id",
        allFriends = [];
    var gatherer = function(gstr) {
      graph.get(gstr, function(err, res) {
        if (err) {
          console.error(err);
          done(err);
        }
        if (res.data.length > 0) {
          
          for (var a = 0; a < res.data.length; a++) {
            var friendItem = res.data[a],
                friendFBID = crypto.createHash('md5').update(friendItem.id).digest('hex').toString(),
                friendObj = { friend_id: friendFBID, count_likes: 0, count_comments: 0, count_posts: 0 };
            allFriends.push(friendObj);
          }
          
          if (res.paging && res.paging.next) {
            gatherer(res.paging.next);
          }
        }
        else {
          // no more paging
          collected.user.friends = allFriends;
          console.log('Finished mining friends.');
          done(err, 'success');
        }
      });
    };
    gatherer(graphStr);
  },
  gatherPosts: function(done) {
    var graphStr = "me/feed?fields=id,from,message,privacy,created_time,updated_time,comments.fields(from,message,id,message_tags,created_time,like_count),likes.fields(id),status_type,to,message_tags,shares,place&since=1&limit=100",
        allData = [];
    var gatherer = function(gstr) {
      graph.get(gstr, function(err, res) {
        if (err) {
          console.error(err);
          return done(err);
        }
        if (res.data.length > 0) {
          // Contraints for if we use the data needs to go here.
          for (var a = 0; a < res.data.length; a++) {
            var feedItem = res.data[a],
                feedObj = {};
            
            if (feedItem.message && feedItem.message.length > 1) {
              var hashedPostId = crypto.createHash('md5').update(feedItem.id).digest('hex').toString(),
                  fromHashedId = crypto.createHash('md5').update(feedItem.from.id).digest('hex').toString();
                  
              var created_timestamp = new Date(feedItem.created_time),
                  updated_timestamp = new Date(feedItem.updated_time);
                  
              // check to see if it was posted longer than 6 months ago
              if (created_timestamp.getTime() < sixMonthsAgo.getTime()) {
                collected.user.posts = allData;
                console.log('Finished mining posts up till 12 months ago');
                return done(err, 'success');
              }
              
              // If from_id is not user's, add to count_posts for the friend_id
              if (fromHashedId !== uidHash) {
                for (var ab = 0; ab < collected.user.friends.length; ab++) {
                  var friendId = collected.user.friends[ab].friend_id;
                  if (friendId === fromHashedId) {
                    collected.user.friends[ab].count_posts += 1;
                  }
                }
              }
              
              // If the post contains any variations of the user's name, remove it
              for (var ac = 0; ac < nameVars.length; ac++) {
                feedItem.message = feedItem.message.replace(nameVars[ac], "", "gi");
              }
              
              feedObj.post_id = hashedPostId;
              feedObj.from_id = fromHashedId;
              feedObj.message = feedItem.message;
              feedObj.privacy = feedItem.privacy.value;
              feedObj.created_time = created_timestamp.getTime();
              feedObj.updated_time = updated_timestamp.getTime();
              feedObj.status_type = feedItem.status_type;
              
              (feedItem.shares) ? feedObj.shares = feedItem.shares.count : feedObj.shares = 0;
              if (feedItem.place) {
                feedObj.place_id = crypto.createHash('md5').update(feedItem.place.id).digest('hex').toString();
              }
              // get like ids into array
              if (feedItem.likes) {
                var likeIds = [];
                for (var b = 0; b < feedItem.likes.data.length; b++) {
                  var likeObj = feedItem.likes.data[b],
                      hashedLikeId = crypto.createHash('md5').update(likeObj.id).digest('hex').toString();
                  // if the hashedLikeId is not the user's id, find the friend_id in 'friends' and add to count_likes
                  if (hashedLikeId !== uidHash) {
                    for (var ba = 0; ba < collected.user.friends.length; ba++) {
                      if (hashedLikeId === collected.user.friends[ba].friend_id) {
                        collected.user.friends[ba].count_likes += 1;
                      }
                    }
                  }
                  likeIds.push(hashedLikeId);
                }
                feedObj.likes = likeIds;
              }
              // Get comments, hash UIDs, convert time to UNIX timestamps, remove tagged users in comments
              if (feedItem.comments) {
                var comments = [];
                for (var c = 0; c < feedItem.comments.data.length; c++) {
                  var commentObj = feedItem.comments.data[c],
                      hashedCommentId = crypto.createHash('md5').update(commentObj.id).digest('hex').toString(),
                      commentCreated_timestamp = new Date(commentObj.created_time),
                      hashedCommentorId;
                      
                  if (commentObj.from && commentObj.from.id !== undefined) {
                    hashedCommentorId = crypto.createHash('md5').update(commentObj.from.id).digest('hex').toString();
                  }
                  else {
                    hashedCommentorId = null;
                  }
                      
                  var cObj = { from_id: hashedCommentorId, comment_id: hashedCommentId, message: commentObj.message, like_count: commentObj.like_count, created_time: commentCreated_timestamp.getTime() };
                  
                  if (commentObj.message && commentObj.message.length > 0) {    
                    if (commentObj.message_tags) {
                      var commentTaggedIds = [],
                          commentTaggedUsers = [];
                      // Remove comment message tags.
                      for (var ca = 0; ca < commentObj.message_tags.length; ca++) {
                        var ctag = commentObj.message_tags[ca],
                            cname = ctag.name,
                            ctagged_id = crypto.createHash('md5').update(ctag.id).digest('hex').toString();
                        commentTaggedIds.push(ctagged_id);
                        commentTaggedUsers.push(cname);
                      }
                      for (var cb = 0; cb < commentTaggedUsers.length; cb++) {
                        cObj.message = cObj.message.replace(commentTaggedUsers[cb], "");
                      }
                      cObj.tagged_ids = commentTaggedIds;
                    }
                    // if hashedCommentorId is not user's id, find friend_id in 'friends' and add to count_comments
                    if (hashedCommentorId !== uidHash) {
                      for (var cc = 0; cc < collected.user.friends.length; cc++) {
                        if (hashedCommentorId === collected.user.friends[cc].friend_id) {
                          collected.user.friends[cc].count_comments += 1;  
                        }
                      }
                    }
                    // If the comment contains any variations of the user's name, remove it
                    for (var cd = 0; cd < nameVars.length; cd++) {
                      cObj.message = cObj.message.replace(nameVars[cd], "", "gi");
                    }
                    comments.push(cObj);
                  }
                }
                // If there were comments.
                if (comments.length > 0) {
                  feedObj.comments = comments;
                }
              }
              // Hashes the to_ids that the post was directed towards
              if (feedItem.to) {
                var toIds = [];
                for (var d = 0; d < feedItem.to.data.length; d++) {
                  var toObj = feedItem.to.data[d],
                      toHashedId = crypto.createHash('md5').update(toObj.id).digest('hex').toString();
                  toIds.push(toHashedId);
                }
                feedObj.to_ids = toIds;
              }
              // Remove tagged user's in message, and add the UID to the tagged_ids Array.
              if (feedItem.message_tags) {
                var taggedIds = [],
                    taggedUsers = [];
                
                for (var e in feedItem.message_tags) {
                  var tag = feedItem.message_tags[e][0],
                      name = tag.name,
                      tagged_id = crypto.createHash('md5').update(tag.id).digest('hex').toString();
                  taggedIds.push(tagged_id);
                  taggedUsers.push(name);
                } 
                feedObj.tagged_ids = taggedIds;
                for (var ea = 0; ea < taggedUsers.length; ea++) {
                  feedObj.message = feedObj.message.replace(taggedUsers[ea], "");
                }
              }
              
              // After removing tags, check to see if message has any content left.
              // If it does, push it.
              if (feedObj.message.trim().length > 0) {
                allData.push(feedObj);
              }
            }
          }
          // While there are still results to get. get the next result set  
          if (res.paging && res.paging.next) {
            gatherer(res.paging.next);
          }
        }
        else {
          // No more pagings / results. That means its done. 
          collected.user.posts = allData;
          console.log('Finished mining posts');
          done(err, 'success');
        }
      });
    };
    gatherer(graphStr);
  }
}, function(err, results) {
  if (err) return console.error('Miner Error: '+err);
  
  var goodFriends = [];
  // Loop through collected.user.friends, making a new friendslist with on
  // friends that have at least 1 with the user.
  for (var a = 0; a < collected.user.friends.length; a++) {
    var friendObj = collected.user.friends[a],
        score = friendObj.count_likes + friendObj.count_comments + friendObj.count_posts;
    if (score > 0) {
      goodFriends.push(friendObj);
    }
  }
  collected.user.friends = goodFriends;
  
  // Sort to have best friends at top of stack
  async.sortBy(collected.user.friends, function(friend, callback) {
    var score = friend.count_likes + friend.count_comments + friend.count_posts;
    callback(null, score);
  }, function(err, result) {
    collected.user.friends = result.reverse();
  });
  
  // Write to AWS S3
  var params = {
    Bucket: 'fbminer',
    Key: s3UserFile,
    ACL: 'public-read-write',
    Body: JSON.stringify(collected, null, 2)
  };
  
  s3.putObject(params, function(err, data) {
    if (err) return console.error('S3 putObject error: '+err);
    console.log("Data written to file : "+s3UserFile+" successfully.");
  });
});
// gcr 4/23/2015 @ 2:41PM
// Changes from this diff were not made on my behalf. You are warned