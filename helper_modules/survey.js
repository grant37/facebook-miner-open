var fs = require('fs');

/**
 * getPageQuestions
 * Gets the questions given the page number
 */
exports.getPageQuestions = function(pageNumber) {
  
  var d = fs.readFileSync('pages.json'); 
  
  if (d) {
    
    var json = JSON.parse(d),
        pages = json.pages;
        
    return pages[pageNumber - 1];
  }
};

/**
 * parseResponse
 * Parse the req.body for the question id's in pages.json
 * 
 * @param responses - Object (Req.body)
 * @param questions - Array - From getPageQuestions
 */
exports.parseResponse = function(responses, questions) {
  
  for (var quid in responses) {
    
    var responseTxt = responses[quid];
    
    for (var a = 0; a < questions.length; a++) {
      var qObj = questions[a],
          qObjID = qObj.QuestionID;
      if (quid === qObjID) {
        
        if (qObj.choices) {
          for (var b = 0; b < qObj.choices.length; b++) {
            if (qObj.choices[b] == responseTxt) {
              qObj.selectedValue = b+1;
              qObj.selectedText = responseTxt;
            }
          }
        }
        else {
          qObj.selectedText = responseTxt;
        }
        
      }
    }
  }
  
  // put NR for questions w/o selectedValue/Text
  for (var c = 0; c < questions.length; c++) {
    var q = questions[c];
    
    if (!q.selectedValue && !q.selectedText) {
      q.selectedValue = false;
      q.selectedText = false;
    }
  }
  
  return questions;
};