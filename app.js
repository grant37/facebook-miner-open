/**
 * Module dependencies.
 */
var express = require('express'),
    MongoStore = require("connect-mongo")(express),
    path = require('path'),
    mongoose = require("mongoose"),
    Login = require('./models/Login'),
    passport = require("passport");

/**
 * Controllers
 */
var baseController = require('./controllers/base'),
    apiController = require('./controllers/api');


/**
 * API
 */
var secrets = require("./config/secrets"),
    passportConf = require("./config/passport");

/**
 * Express
 */
var app = express();

/**
 * Mongo / Mongoose
 */
mongoose.connect(secrets.db);
mongoose.connection.on("error", function() {
  console.error("MongoDB connection Error: Make sure MongoDB is running.");
});
app.enable('trust proxy');
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(require('connect-assets')({
  src: 'public',
  helperContext: app.locals
}));
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.cookieParser('YouShallNotPassWord'));
/**
 * Will Be Removed in connect 3.0
 * use urlencoded() and json() as replacement
app.use(express.bodyParser());
 */
app.use(express.urlencoded());
app.use(express.json());
app.use(express.methodOverride());
app.use(express.session({
  secret: secrets.sessionSecret,
  store: new MongoStore({
    db: mongoose.connection.db,
    auto_reconnect: true
  })
}));
app.use(express.csrf());
app.use(passport.initialize());
app.use(passport.session());
app.use(function(req, res, next) {
  res.locals.user = req.user;
  res.locals.token = req.csrfToken();
  res.locals.secrets = secrets;
  next();
});
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}


/**
 * Logging Middleware
 */
var logBasicAuth = function(req, res, next) {
  var ip = req.ip;
  
  var log = new Login({
    ip_address: ip
  });
  
  log.save(function(err) {
    if (err) {
      console.error(err);
      return next(err);
    }
    return next();
  });
};

/**
 * Express Routes
 */
app.get("/", baseController.getHome);
app.get("/start", passportConf.isAuthenticated, passportConf.isAuthorized, baseController.getStart);
app.get("/callback", passportConf.isAuthenticated, passportConf.isAuthorized, baseController.getSurveyComplete);
app.get("/success", passportConf.isAuthenticated, passportConf.isAuthorized, baseController.getSuccess);
app.get("/error", passportConf.isAuthenticated, passportConf.isAuthorized, baseController.getError);
app.get("/results", passport.authenticate('basic', { session: false}), logBasicAuth, baseController.getResults);
app.get("/results/export", passport.authenticate('basic', { session: false}), baseController.exportData);
app.get("/results/:awsKey", passport.authenticate('basic', { session: false}), baseController.getResultByKey);
app.get("/logout", baseController.getLogout);
app.get("/survey", passportConf.isAuthenticated, passportConf.isAuthorized, baseController.getSurvey);
app.post("/survey", passportConf.isAuthenticated, passportConf.isAuthorized, baseController.postSurvey);
app.get("/deauth", baseController.getDeauth);
app.get("/finish", baseController.getFinish);
app.get("/fix", baseController.doFix); // Dev Fix to combine data


/**
 * OAuth routes
 */
app.get("/auth/facebook", passport.authenticate("facebook", { scope: ['public_profile', 'user_friends', 'user_status', 'read_stream', 'user_about_me', 'user_religion_politics', 'user_relationships', 'user_location']}));
app.get("/auth/facebook/callback", passport.authenticate("facebook", { successRedirect: '/start', failureRedirect: '/logout'}));


/**
 * Start Server
 */
var server = require('http').createServer(app);

server.listen(app.get('port'), function() {
  console.log("Express server listening on port %d in %s mode", app.get('port'), app.settings.env);
});

/**
 * Socket IO
 */
var io = require('socket.io').listen(server);
io.enable('browser client minification');
io.enable('browswer client etag');
io.set('log level', 1);
io.sockets.on('connection', function(s) {
  s.on('start', function(data) {
    var uidHash = data.uid,
        token = data.token;
    apiController.miner(uidHash, token, s);
  });
  s.on('restartMine', function(data) {
    var uidHash = data.uid,
        token = data.token;
    apiController.miner(uidHash, token, s);
  });
  s.on('minerFinish', function(data) {
    var uidHash = data.uid;
    
    if (apiController.updateMiner(uidHash)) {
      console.log("miner status update finished");
    }
  });
  s.on('surveyFinished', function(data) {
    var uidHash = data.uid, completed = false;
    if (apiController.updateSurvey(uidHash)) {
      console.log("survey status update finished");
      if (apiController.completed(uidHash)) {
        completed = true;
      }
    }
    s.broadcast.emit('surveyFinish', { uid: data.uid, completed: completed });
  });
  s.on('isComplete', function(data) {
    var uidHash = data.uid;
    if (apiController.completed(uidHash)) {
      if (apiController.combineData(data.id, uidHash)) {
        s.emit('status', { completed: true });
      }
      else {
        s.emit('status', { completed: true, error: 'combining data' });
      }
    }
    else {
      s.emit('status', { completed: false });
    }
  });
});